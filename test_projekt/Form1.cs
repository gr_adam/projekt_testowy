﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test_projekt
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn_przycisk_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2(tbx_imie.Text, tbx_nazwisko.Text, cmbx_lista.SelectedItem.ToString(), rbtn_plec.Checked ? 0 : 1, chkbx_prJazdy.Checked);
            form2.Show();
        }
    }
}
