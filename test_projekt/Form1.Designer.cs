﻿namespace test_projekt
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.ladna_nazwa = new System.Windows.Forms.Label();
            this.druga_nazwa = new System.Windows.Forms.Label();
            this.trzecia_nazwa = new System.Windows.Forms.Label();
            this.czw_nazwa = new System.Windows.Forms.Label();
            this.piata_nazwa = new System.Windows.Forms.Label();
            this.tbx_imie = new System.Windows.Forms.TextBox();
            this.tbx_nazwisko = new System.Windows.Forms.TextBox();
            this.cmbx_lista = new System.Windows.Forms.ComboBox();
            this.rbtn_plec = new System.Windows.Forms.RadioButton();
            this.plec = new System.Windows.Forms.Label();
            this.rdbtn_plec2 = new System.Windows.Forms.RadioButton();
            this.chkbx_prJazdy = new System.Windows.Forms.CheckBox();
            this.btn_przycisk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ladna_nazwa
            // 
            this.ladna_nazwa.AutoSize = true;
            this.ladna_nazwa.Location = new System.Drawing.Point(143, 96);
            this.ladna_nazwa.Name = "ladna_nazwa";
            this.ladna_nazwa.Size = new System.Drawing.Size(67, 17);
            this.ladna_nazwa.TabIndex = 0;
            this.ladna_nazwa.Text = "formularz";
            // 
            // druga_nazwa
            // 
            this.druga_nazwa.AutoSize = true;
            this.druga_nazwa.Location = new System.Drawing.Point(143, 153);
            this.druga_nazwa.Name = "druga_nazwa";
            this.druga_nazwa.Size = new System.Drawing.Size(33, 17);
            this.druga_nazwa.TabIndex = 1;
            this.druga_nazwa.Text = "imie";
            // 
            // trzecia_nazwa
            // 
            this.trzecia_nazwa.AutoSize = true;
            this.trzecia_nazwa.Location = new System.Drawing.Point(143, 206);
            this.trzecia_nazwa.Name = "trzecia_nazwa";
            this.trzecia_nazwa.Size = new System.Drawing.Size(65, 17);
            this.trzecia_nazwa.TabIndex = 2;
            this.trzecia_nazwa.Text = "nazwisko";
            // 
            // czw_nazwa
            // 
            this.czw_nazwa.AutoSize = true;
            this.czw_nazwa.Location = new System.Drawing.Point(143, 272);
            this.czw_nazwa.Name = "czw_nazwa";
            this.czw_nazwa.Size = new System.Drawing.Size(77, 17);
            this.czw_nazwa.TabIndex = 3;
            this.czw_nazwa.Text = "stanowisko";
            // 
            // piata_nazwa
            // 
            this.piata_nazwa.AutoSize = true;
            this.piata_nazwa.Location = new System.Drawing.Point(143, 377);
            this.piata_nazwa.Name = "piata_nazwa";
            this.piata_nazwa.Size = new System.Drawing.Size(83, 17);
            this.piata_nazwa.TabIndex = 4;
            this.piata_nazwa.Text = "prawo jazdy";
            // 
            // tbx_imie
            // 
            this.tbx_imie.Location = new System.Drawing.Point(234, 147);
            this.tbx_imie.Name = "tbx_imie";
            this.tbx_imie.Size = new System.Drawing.Size(100, 22);
            this.tbx_imie.TabIndex = 5;
            // 
            // tbx_nazwisko
            // 
            this.tbx_nazwisko.Location = new System.Drawing.Point(234, 201);
            this.tbx_nazwisko.Name = "tbx_nazwisko";
            this.tbx_nazwisko.Size = new System.Drawing.Size(100, 22);
            this.tbx_nazwisko.TabIndex = 6;
            // 
            // cmbx_lista
            // 
            this.cmbx_lista.FormattingEnabled = true;
            this.cmbx_lista.Items.AddRange(new object[] {
            "zbrojarz",
            "tynkarz",
            "malarz",
            "dźwigowy",
            "kierowca"});
            this.cmbx_lista.Location = new System.Drawing.Point(234, 265);
            this.cmbx_lista.Name = "cmbx_lista";
            this.cmbx_lista.Size = new System.Drawing.Size(121, 24);
            this.cmbx_lista.TabIndex = 7;
            // 
            // rbtn_plec
            // 
            this.rbtn_plec.AutoSize = true;
            this.rbtn_plec.Location = new System.Drawing.Point(256, 327);
            this.rbtn_plec.Name = "rbtn_plec";
            this.rbtn_plec.Size = new System.Drawing.Size(40, 21);
            this.rbtn_plec.TabIndex = 8;
            this.rbtn_plec.TabStop = true;
            this.rbtn_plec.Text = "m";
            this.rbtn_plec.UseVisualStyleBackColor = true;
            // 
            // plec
            // 
            this.plec.AutoSize = true;
            this.plec.Location = new System.Drawing.Point(143, 329);
            this.plec.Name = "plec";
            this.plec.Size = new System.Drawing.Size(34, 17);
            this.plec.TabIndex = 9;
            this.plec.Text = "plec";
            // 
            // rdbtn_plec2
            // 
            this.rdbtn_plec2.AutoSize = true;
            this.rdbtn_plec2.Location = new System.Drawing.Point(319, 327);
            this.rdbtn_plec2.Name = "rdbtn_plec2";
            this.rdbtn_plec2.Size = new System.Drawing.Size(36, 21);
            this.rdbtn_plec2.TabIndex = 10;
            this.rdbtn_plec2.TabStop = true;
            this.rdbtn_plec2.Text = "k";
            this.rdbtn_plec2.UseVisualStyleBackColor = true;
            // 
            // chkbx_prJazdy
            // 
            this.chkbx_prJazdy.AutoSize = true;
            this.chkbx_prJazdy.Location = new System.Drawing.Point(256, 372);
            this.chkbx_prJazdy.Name = "chkbx_prJazdy";
            this.chkbx_prJazdy.Size = new System.Drawing.Size(18, 17);
            this.chkbx_prJazdy.TabIndex = 11;
            this.chkbx_prJazdy.UseVisualStyleBackColor = true;
            // 
            // btn_przycisk
            // 
            this.btn_przycisk.Location = new System.Drawing.Point(611, 265);
            this.btn_przycisk.Name = "btn_przycisk";
            this.btn_przycisk.Size = new System.Drawing.Size(75, 23);
            this.btn_przycisk.TabIndex = 12;
            this.btn_przycisk.Text = "Zakoncz";
            this.btn_przycisk.UseVisualStyleBackColor = true;
            this.btn_przycisk.Click += new System.EventHandler(this.btn_przycisk_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_przycisk);
            this.Controls.Add(this.chkbx_prJazdy);
            this.Controls.Add(this.rdbtn_plec2);
            this.Controls.Add(this.plec);
            this.Controls.Add(this.rbtn_plec);
            this.Controls.Add(this.cmbx_lista);
            this.Controls.Add(this.tbx_nazwisko);
            this.Controls.Add(this.tbx_imie);
            this.Controls.Add(this.piata_nazwa);
            this.Controls.Add(this.czw_nazwa);
            this.Controls.Add(this.trzecia_nazwa);
            this.Controls.Add(this.druga_nazwa);
            this.Controls.Add(this.ladna_nazwa);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ladna_nazwa;
        private System.Windows.Forms.Label druga_nazwa;
        private System.Windows.Forms.Label trzecia_nazwa;
        private System.Windows.Forms.Label czw_nazwa;
        private System.Windows.Forms.Label piata_nazwa;
        private System.Windows.Forms.TextBox tbx_imie;
        private System.Windows.Forms.TextBox tbx_nazwisko;
        private System.Windows.Forms.ComboBox cmbx_lista;
        private System.Windows.Forms.RadioButton rbtn_plec;
        private System.Windows.Forms.Label plec;
        private System.Windows.Forms.RadioButton rdbtn_plec2;
        private System.Windows.Forms.CheckBox chkbx_prJazdy;
        private System.Windows.Forms.Button btn_przycisk;
    }
}

