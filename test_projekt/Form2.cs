﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test_projekt
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(string imie,string nazwisko,string stanowisko,int plec,bool czyPrawoJazdy)
        {
            InitializeComponent();
            lbl1.Text = imie;
            lbl2.Text = nazwisko;
            lbl3.Text = stanowisko;
            if (plec == 0) { lbl4.Text = "mężczyzna"; }
                else { lbl4.Text = "kobieta"; }
            if (czyPrawoJazdy) { lbl5.Text = "Posiada prawojazdy"; }
                else { lbl5.Text = "Nie posiada prawojazdy"; }

        }
    }
}
